import random

# Play the standard columns
numbersList = []
for i in range(1, 51, 10):
    numbersList.append(i)
for i in range(2, 51, 10):
    numbersList.append(i)
for i in range(3, 51, 10):
    numbersList.append(i)
for i in range(4, 51, 10):
    numbersList.append(i)
for i in range(5, 51, 10):
    numbersList.append(i)
for i in range(6, 51, 10):
    numbersList.append(i)
for i in range(7, 51, 10):
    numbersList.append(i)
for i in range(8, 51, 10):
    numbersList.append(i)
for i in range(9, 51, 10):
    numbersList.append(i)
for i in range(10, 51, 10):
    numbersList.append(i)

for i in range(31, 81, 10):
    numbersList.append(i)
for i in range(32, 81, 10):
    numbersList.append(i)
for i in range(33, 81, 10):
    numbersList.append(i)
for i in range(34, 81, 10):
    numbersList.append(i)
for i in range(35, 81, 10):
    numbersList.append(i)
for i in range(36, 81, 10):
    numbersList.append(i)
for i in range(37, 81, 10):
    numbersList.append(i)
for i in range(38, 81, 10):
    numbersList.append(i)
for i in range(39, 81, 10):
    numbersList.append(i)
for i in range(40, 81, 10):
    numbersList.append(i)

# Print the standard columns in groups of 5
plusNumbers = 0
print("\nThe initial numbers are: ")
for i in range(0, len(numbersList)):
    print(format(numbersList[i], "02d"), end=" ")
    plusNumbers += 1
    if plusNumbers % 5 == 0:
        print("")

# We have played 100 numbers at this point
# We need to select 50 more numbers
totalNumbersPlayed = len(numbersList)
maxNumbers = 150
plusNumbers = 0

# Print the random numbers in groups of 5
print("\nThe random numbers are: ")

while totalNumbersPlayed < maxNumbers:

    # Choose a random number between 1 and 80
    randomNum = random.randrange(1, 80, 1)

    # Check how many times the random number exists in the list
    occurrences = numbersList.count(randomNum)

    # If it exists only once, add it to the list
    if occurrences < 2:
        numbersList.append(randomNum)
        print(format(randomNum, "02d"), end=" ")
        totalNumbersPlayed += 1
        plusNumbers += 1

        if plusNumbers % 5 == 0:
            print("")

print("\nTotal numbers played:", totalNumbersPlayed)

